import datetime


def datetime_ago(**kw):
    now = datetime.datetime.now()
    delta = datetime.timedelta(**kw)
    return now - delta

def format_datetime(time):    
    return time.strftime("%Y-%m-%d %H:%M")

def format_rel_datetime(time, now=None):
    """
    >>> format_rel_datetime(datetime.datetime(2011, 10, 5, 10, 0, 0, 0), datetime.datetime(2011, 10, 5, 11, 0, 5, 0))
    '1h ago'
    >>> format_rel_datetime(datetime.datetime(2011, 10, 5, 10, 0, 0, 0), datetime.datetime(2011, 10, 5, 11, 5, 5, 0))
    '1h, 5m ago'
    >>> format_rel_datetime(datetime.datetime(2011, 9, 10, 10, 0, 0, 0), datetime.datetime(2011, 10, 5, 11, 5, 5, 0))
    '3w, 4d ago'
    >>> format_rel_datetime(datetime.datetime(2011, 9, 14, 10, 0, 0, 0), datetime.datetime(2011, 10, 5, 11, 5, 5, 0))
    '3w ago'
    """
    if now is None:
        now = datetime.datetime.now()
    
    delta = now - time
    
    weeks, days = divmod(delta.days, 7)
    hours, seconds = divmod(delta.seconds, 60*60)
    minutes, seconds = divmod(seconds, 60)
    
    results = []
    if weeks:
        results.append(('w', weeks))
    if days:
        results.append(('d', days))
    if hours and not weeks:
        results.append(('h', hours))
    if minutes and not (weeks or days):
        results.append(('m', minutes))
    if seconds and not (weeks or days or hours):
        results.append(('s', seconds))
    
    
    
    if len(results) >= 2:
        return '%d%s, %d%s ago' % (results[0][1], results[0][0], results[1][1], results[1][0])
    if len(results) == 1:
        return '%d%s ago' % (results[0][1], results[0][0])
    
    return 'now'
    