from flask.ext.testing import TestCase

from kickerscore import create_app, db
from kickerscore.model import Player, Match

class MyTest(TestCase):

    SQLALCHEMY_DATABASE_URI = "sqlite://"
    TESTING = True

    def create_app(self):
        return create_app(self)

    def test_scores(self):
        m = Match(red1=self.bob, blue1=self.alice, score_red=5,  score_blue=2)
        m.insert_scores()
        db.session.add(m)
        resp = self.client.get('/scores')
        assert 'Bob' in resp.data

    def setUp(self):
        db.create_all()
        self.bob = Player('Bob')
        self.alice = Player('Alice')
        db.session.add_all([self.bob, self.alice])
        

    def tearDown(self):

        db.session.remove()
        db.drop_all()