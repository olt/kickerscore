from __future__ import division

def ea(ra, rb):
    diff = rb-ra
    if diff < -400: diff = -400
    if diff > 400: diff = 400
    return 1.0/(1 + 10**(diff/400.0))

def new_rating(ra, rb, k, sa):
    return ra + k * (sa - ea(ra, rb))

def update_match_elo(match):
    if match.red2 is None:
        update_single_elo(match)
    else:
        update_off_def_elo(match)
        update_double_elo(match)

KVALUE = 30
INITIAL_ELO = 1000

def update_off_def_elo(match):
    elos_def = {}
    elos_off = {}
    for score in match.scores:
        if score.won:
            sa = 1.0
        elif score.lost:
            sa = 0.0
        else:
            continue
        if score.position == score.Position.OFF:
            elos_off[score.player] = new_rating(
                score.player.elo_off or INITIAL_ELO,
                score.opponent.elo_def or INITIAL_ELO,
                KVALUE, sa,
            )
        elif score.position == score.Position.DEF:
            elos_def[score.player] = new_rating(
                score.player.elo_def or INITIAL_ELO,
                score.opponent_teammate.elo_off or INITIAL_ELO,
                KVALUE, sa,
            )

    for player, elo in elos_def.iteritems():
        player.elo_def = elo
    for player, elo in elos_off.iteritems():
        player.elo_off = elo

def update_single_elo(match):
    new_elos = {}
    for score in match.scores:
        if score.won:
            sa = 1.0
        elif score.lost:
            sa = 0.0
        else:
            continue
        new_elos[score.player] = new_rating(
            score.player.elo_single or INITIAL_ELO,
            score.opponent.elo_single or INITIAL_ELO,
            KVALUE, sa,
        )
    for player, elo in new_elos.iteritems():
        player.elo_single = elo

def update_double_elo(match):
    new_elos = {}
    for score in match.scores:
        if score.won:
            sa = 1.0
        elif score.lost:
            sa = 0.0
        else:
            continue

        elo_team = ((score.player.elo_double or INITIAL_ELO) +
            (score.teammate.elo_double or INITIAL_ELO)) / 2
        elo_opponent = ((score.opponent.elo_double or INITIAL_ELO) +
            (score.opponent_teammate.elo_double or INITIAL_ELO)) / 2
        elo_diff = new_rating(
            elo_team, elo_opponent,
            KVALUE, sa,
        ) - elo_team

        new_elo = (score.player.elo_double or INITIAL_ELO) + elo_diff
        new_elos[score.player] = new_elo

    for player, elo in new_elos.iteritems():
        player.elo_double = elo

